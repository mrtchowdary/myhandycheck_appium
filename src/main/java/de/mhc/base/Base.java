package de.mhc.base;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class Base {

	public static AppiumDriver<MobileElement> driver;
	
	@BeforeClass//(alwaysRun=true)
	public void setUpClass(){
		try{
			DesiredCapabilities dc = new DesiredCapabilities();
			dc.setCapability("deviceName", "Nokia 6.1 plus");
			dc.setCapability("udid", "DRGID18082602064");
			dc.setCapability("platformName", "Android");
			dc.setCapability("platformVersion", "10.0");
			dc.setCapability("appPackage", "de.webtogo.myhandycheck.debug");
			dc.setCapability("appActivity", "de.webtogo.myhandycheck.SplashActivity");

			URL url = new URL("http://0.0.0.0:4723/wd/hub");
			driver = new AppiumDriver<MobileElement>(url,dc);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}catch(Exception exc){
			System.out.println(exc.getCause());
			System.out.println(exc.getMessage());
		}
	}
	
	@AfterClass(alwaysRun=true)
	public void tearDown(){
		driver.quit();
	}
	
//	@Test
//	public void browserPractice() throws Exception{
//		
//		DesiredCapabilities dc = new DesiredCapabilities();		
//		dc.setCapability(MobileCapabilityType.DEVICE_NAME, "Nokia 6.1 plus");
//		dc.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
//		dc.setCapability(MobileCapabilityType.BROWSER_NAME,"Chrome");
//		dc.setCapability("chromedriverExecutable", "E:\\Downloads\\chromedriver.exe");
//		URL url = new URL("http://0.0.0.0:4723/wd/hub");
//		WebDriver driver = new AndroidDriver<WebElement>(url, dc);
//		driver.get("http://automationpractice.com/index.php");
//		System.out.println("Screen title is : "+ driver.getTitle());
//	}
}
