package de.mhc.screens;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

import java.util.List;

import org.openqa.selenium.support.PageFactory;

import de.mhc.base.Base;

public class HomeScreen extends Base{
	
	@AndroidFindBy(id="de.webtogo.myhandycheck.debug:id/main_logo")
	MobileElement homeLogo;
	
	@AndroidFindBy(className="android.widget.ImageButton")
	MobileElement burgerMenu;
	
	@AndroidFindBy(id="de.webtogo.myhandycheck.debug:id/button")
	List<MobileElement> homeButton;
	
	@AndroidFindBy(className="android.widget.TextView")
	List<MobileElement> notYetReadyButton;
	
	@AndroidFindBy(id="de.webtogo.myhandycheck.debug:id/button")
	MobileElement gotItLetsStartButton;
	
	@AndroidFindBy(id="de.webtogo.myhandycheck.debug:id/button")
	MobileElement checkMyPhonePR2;
	
	@AndroidFindBy(id="de.webtogo.myhandycheck.debug:id/enabled_switch")
	MobileElement wiFiBluetoothSlider;
	
	public HomeScreen(){
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	public boolean isHomeScreen(){
		return homeLogo.isDisplayed();
//		return driver.findElement(By.id("de.webtogo.myhandycheck.debug:id/main_logo")).isDisplayed();
	}
	
	public void clickOnMenu(){
		burgerMenu.click();
//		driver.findElement(By.className("android.widget.ImageButton")).click();
	}
	
	public String getCheckMyPhoneButtonText(){
		return homeButton.get(0).getText();
	}
	
	public void clickOnCheckMyPhoneButton(){
		homeButton.get(0).click();
	}
	
	public String getBackupTransferButtonText(){
		return homeButton.get(1).getText();
	}
	
	public void clickOnBackupTransferButton(){
		homeButton.get(1).click();
	}
	
	public String getIAmNotYetReadyText(){
		return notYetReadyButton.get(notYetReadyButton.size()-1).getText();
	}
	
	public void clickOnIAmNotYetReady(){
		notYetReadyButton.get(notYetReadyButton.size()-1).click();
	}
	
	public String getGotItLetsStartButtonText(){
		return gotItLetsStartButton.getText();
	}
	
	public void clickOnGotItLetsStartButton(){
		gotItLetsStartButton.click();
	}
	
	public boolean checkIfCheckMyPhoneButtonEnabledPR2(){
		return checkMyPhonePR2.isEnabled();
	}
	
	public boolean isWiFiBluetoothSliderOn(){
		return wiFiBluetoothSlider.isSelected();
	}
	
	public void clickOnWiFiBluetoothSlider(){
		wiFiBluetoothSlider.click();
	}
	
//	public void toogleAirplane(){
//		driver.
//	}
}
