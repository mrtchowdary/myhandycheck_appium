package de.mhc.test;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import de.mhc.base.Base;
import de.mhc.screens.HomeScreen;

public class HomeScreenTests extends Base{

	HomeScreen homeScreen;
	
	@BeforeMethod//(alwaysRun=true)
	public void setUp(){
		homeScreen = new HomeScreen();
	}
	
	@Test
	public void StartCheckMyPhone(){
		System.out.println("Checking if home screen");
		Assert.assertTrue(homeScreen.isHomeScreen());
		System.out.println("Click on \"Check my phone\" button");
		homeScreen.clickOnCheckMyPhoneButton();
		System.out.println("Click on \"I\'m not ready yet\"");
		homeScreen.clickOnIAmNotYetReady();
		System.out.println("Checking if home screen");
		Assert.assertTrue(homeScreen.isHomeScreen());
		System.out.println("Click on \"Check my phone\" button");
		homeScreen.clickOnCheckMyPhoneButton();
		System.out.println("Got it, let\'s start");
		homeScreen.clickOnGotItLetsStartButton();
	}
}
